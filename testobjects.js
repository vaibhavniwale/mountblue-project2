const {keys,values,mapobject,pairs,invert,defaults} = require('./objects.js')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
console.log('----keys()-----')
console.log(keys(testObject))
console.log('----.keys()-----')
console.log(Object.keys(testObject))
console.log('----values()-----')
console.log(values(testObject))
console.log('----.values()-----')
console.log(Object.values(testObject))
function abc(num){
    return num + 3; 
}
console.log('----mapObject()-----')
console.log(mapobject(testObject,abc))
console.log('----pairs()-----')
console.log(pairs(testObject))
console.log('----invert()-----')
console.log(invert(testObject))
var iceCream = {flavor: "chocolate"};
console.log('----defaults()-----')
console.log(defaults(iceCream, {flavor: "vanilla", sprinkles: "lots"}));
