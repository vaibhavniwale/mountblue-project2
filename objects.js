function keys(arr){
    return Object.getOwnPropertyNames(arr)
}

function values(arr){
    var v = []
    var a = keys(arr)
    for(i in a){
        v.push(arr[a[i]])
    }
    return v
}

function mapobject(arr,cb){
    var b = {}
    var a = keys(arr)
    for(i in a){
        b[a[i]] = cb(arr[a[i]])
    }
    return b
}

function pairs(arr){
    var v = []
    var a = keys(arr)
    for(i in a){
        v.push([a[i],arr[a[i]]])
    }
    return v
}

function invert(arr){
    var b = {}
    var a = keys(arr)
    for(i in a){
        b[arr[a[i]]] = a[i];
    }
    return b
}

function defaults(arr, c){
    var b = c
    var a = keys(arr)
    for(i in a){
        b[a[i]] = arr[a[i]]
    }
    return b
}

module.exports = {keys,values,mapobject,pairs,invert,defaults}