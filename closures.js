function counterFactory() {
    this.a = 0
    function abc(b){
        this.a = this.a+b
        return this.a
    }
    return {increment: function() {return abc(1)}, decrement: function() {return abc(-1)}}
}

function limitFunctionCallCount(cb,a){
    this.n = a
    console.log(this.n)
    this.runFunction = function(a = 0){
        this.n = this.n + a
        if(this.n){
            this.n = this.n - 1
            //console.log(cb)
            return cb()
        }else{
            return null
        }
    }
}

function cacheFunction(cb){
    let cache = {};
    return function(...args){
        let c = Object.keys(cache)
            .filter(function(a){
                return Array(a).join() === args.join()
            })
            .map(function(a){
                return cache[a]
            })
        if(c.length){
            return c[0]
        }
        let q = cb(...args)
        cache[args] = q        
        return q
    }
}

function sum(arr){
    let su = 0
    for(i in arr){
        su = su + arr[i]
    }
    return su
}

module.exports = {counterFactory,limitFunctionCallCount,cacheFunction}
