const {each,map,reduce,find,filter,flatten} = require('./arrays.js')
const items = [1, 2, 3, 4, 5, 5];
console.log('----each()-----')
each(items,function(word) {
    console.log(word)});
console.log('----.forEach()-----')
items.forEach(function(word) {
    console.log(word)})
console.log('----map()-----')
a = map(items,function(num){ return num * 3; })
console.log(a);
function abc(num){
    return num * 3; 
}
console.log('----.map()-----')
a = items.map(num => abc(num))
console.log(a)
console.log('----reduce()-----')
a = reduce(items,abc,5)
console.log(a);
console.log('----.reduce()-----')
a = items.reduce(num => abc(num),5)
console.log(a)
console.log('----find()-----')
a = find(items,element => element > 4)
console.log(a);
console.log('----.find()-----')
a = items.find(element => element > 4)
console.log(a);
console.log('----filter()-----')
a = filter(items,element => element > 4)
console.log(a);
console.log('----.filter()-----')
a = items.filter(element => element > 4)
console.log(a)
console.log('----flatten()-----')
console.log(flatten([1, [2], [[3]], [[[4]]]]))
console.log(flatten([1, [2], [[3]], [[[4]]]]))
